﻿using UnityEngine;
using System.Collections;
using Burst;

public class Controller : MonoBehaviour {

	void Awake () {

	}

	void OnFoundGraphic (structFoundGraphics sfg) {

		Singleton.Instance.OnFoundGraphic -= OnFoundGraphic;
	}

	public void ClickBtn(int i){
		switch(i){
		case 1:
			Singleton.Instance.OnFoundGraphic += OnFoundGraphic;
			Texture2D tex = Resources.Load("test") as Texture2D;
			byte[] byteGraphic = tex.EncodeToJPG();
			Singleton.Instance.SaveGraphic("test.jpg", byteGraphic);
			break;
		case 2:
			Singleton.Instance.OnFoundGraphic += OnFoundGraphic;

			break;
		}
	}

}
