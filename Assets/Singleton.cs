﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Burst;

public class Singleton : MonoBehaviour {
	// Event Handler
	public delegate void OnFoundGraphicEvent(structFoundGraphics sfg);
	public event OnFoundGraphicEvent OnFoundGraphic;

	static Singleton instance;
	//List<structFoundGraphics> lstFound;
	Singleton(){
		//lstFound = new List<structFoundGraphics>();
	}
	//  Instance 	
	public static Singleton Instance 	
	{ 		
		get 		
		{ 			
			if (instance ==  null){
				instance = GameObject.FindObjectOfType(typeof(Singleton)) as  Singleton; 
			}
			return instance;	
		}			
	}

	public void FindGraphic(structFoundGraphics sfg){
		if(sfg.Target != null && sfg.FileName != ""){
			structFoundGraphics sfgReturn = new structFoundGraphics();
			//is file exist in Resources?
			Sprite s = Resources.Load("test") as Sprite;
			if(s == null){
				//is file exist in folder?
				string realPath = Application.persistentDataPath + "/Graphics/Mugshot/test.jpg";

				FileInfo fInfo = new FileInfo(realPath);
				if (!fInfo.Exists)
				{
					//request server to send it back
					Debug.Log("Request server to send it back");

				}else{
					// Open the file.
					FileStream fStream = new FileStream(realPath, FileMode.Open);
					
					// Create a buffer.
					sfgReturn.Graphic = new byte[fStream.Length];
					
					// Read the file contents to the buffer.
					fStream.Read(sfgReturn.Graphic, 0, (int)fStream.Length);
					
					sfgReturn.FileName = realPath;
					sfgReturn.Path = FoundLocation.Folder;
					sfgReturn.Target = sfg.Target;
					sfgReturn.sGraphic = null;
					OnFoundGraphic(sfgReturn);
				}
				


			}else{
				sfgReturn.FileName = sfg.FileName;
				sfgReturn.Path = FoundLocation.Resource;
				sfgReturn.Target = sfg.Target;
				sfgReturn.sGraphic = s;
				sfgReturn.Graphic = null;
				OnFoundGraphic(sfgReturn);
			}
		}
	}

	public void SaveGraphic(string strFileName, byte[] byteGraphic){
		string realPath = Application.persistentDataPath + "/Graphics/Mugshot/" + strFileName;
		if (!Directory.Exists(Application.persistentDataPath + "/Graphics/Mugshot/"))
		{
			Directory.CreateDirectory(Application.persistentDataPath + "/Graphics/Mugshot/");
		}

		FileStream fStream = new FileStream(realPath,FileMode.Create ,FileAccess.Write);
		fStream.Write(byteGraphic, 0, byteGraphic.Length);
	}
}
