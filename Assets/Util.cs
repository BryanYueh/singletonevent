﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Burst{
	public enum FoundLocation {Resource, Folder, NG}
	public struct structFoundGraphics{
		FoundLocation ePath;
		string strFileName;
		Image imgTarget;
		Sprite spriteGraphic;
		byte[] byteGraphic;

		public FoundLocation Path{
			get{return ePath;}
			set{ePath = value;}
		}
		public string FileName{
			get{return strFileName;}
			set{strFileName = value;}
		}
		public Image Target{
			get{return imgTarget;}
			set{imgTarget = value;}
		}
		public Sprite sGraphic{
			get{return spriteGraphic;}
			set{spriteGraphic = value;}
		}
		public byte[] Graphic{
			get{return byteGraphic;}
			set{byteGraphic = value;}
		}

	}

	public class Util {

	}
}
